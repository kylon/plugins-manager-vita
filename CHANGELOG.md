## Plugins Manager

### 4.0

* Rewritten from scratch
* New UI
* Faster

### 2.5

* Add support for xmc0 partition
* Fix _Disable Plugin_ option
* Fix input lag
* Other fixes
* Code clenup

### 2.03

* Show a reboot aler when disabling kernel plugins
* Minor cleanup

### 2.01

* Minor UI fix

### 2.0

* Add a simple file browser (extension filter: .suprx|.skprx)
* Enhance _Delete Plugin_ option
* Fixes and improvements

### 1.4.2

* Fix plugins list not updating when a plugin is deleted
* Fix _Disable plugin_ option for henkaku.suprx

### 1.4.1

* Minor adjustments

### 1.4

* Multilanguage (See README) [Available: ENG, JAP, ITA]
* Swap X/O when Japanese is the system language
* Show titles for PSP/PSX games
* Add ability to select multiple Title IDs (Square button)
* Automatically add kernel plugins to *KERNEL section (No Title ID selection)
* Add initial support for any plugin path (view only)
* Reload taiHEN config on save
* Add dialogs
* Fixes
* Code clean up

### 1.3

* Re-add support for Adrenaline 5.1<
* Add Halt Points support
* Code clean up

### 1.2

* Add support for Adrenaline 6 (older versions are no longer supported)

### 1.1

* Fix _Disable Plugin_
* Add _Move up_/_Move down_ menu options to order plugins and sections
* Code clean up

### 1.0-1

* Fixes

### 1.0

* Initial release
