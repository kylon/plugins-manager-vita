### Plugins Manager

A plugins manager for PS Vita.

![](plgm.jpg)

## Usage

Changes are **not** saved automatically, you must open the menu and select _**Save changes**_ once done.

TaiHEN config is automatically reloaded on save, but a reboot is still required for kernel plugins.

Put all your plugins in **ur0:tai/plugins** folder if you want to use **Plugins Manager smart features**.

Support for deprecated **ux0:tai** has been removed, please use **ur0:tai**.


## Commands

L / R - Switch mode

Left / Right - Switch section [TaiHen Config Mode only]

Triangle - Toggle menu


## How to build

```$ mkdir build```

```$ cd build```

```$ cmake ..```

```$ make```

## Useful links
* [taiHen library](https://forum.devchroma.nl/index.php?topic=334.0)
* [taiHEN config format](https://github.com/DaveeFTW/taihen-parser/tree/master)


## Credits
xerpi: vita2d

Team Molecule and its contributors: vitaSDK

TheFlow: vitaShell code

VitaSmith: sqlite3
