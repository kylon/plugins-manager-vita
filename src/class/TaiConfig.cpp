/*
	Plugins Manager
	Copyright (C) 2017-2022, kylon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <fstream>

#include "../include/utils.h"

TaiConfig::TaiSection::TaiSection(const std::string &name) {
    this->name.assign(name);
    isHalt = name.size() > 2 && name.at(1) == '!';
}

void TaiConfig::TaiSection::addPlugin(const std::string &plugin) {
    if (std::find(pluginList.begin(), pluginList.end(), plugin) != pluginList.end())
        return;

    pluginList.push_back(plugin);
}

void TaiConfig::TaiSection::removePlugin(const std::string &plugin) {
    std::vector<std::string>::iterator it = std::find(pluginList.begin(), pluginList.end(), plugin);

    while (it != pluginList.end()) {
        pluginList.erase(it);

        it = std::find(pluginList.begin(), pluginList.end(), plugin);
    }
}

void TaiConfig::TaiSection::setHaltPoint() {
    if (isHalt)
        return;

    name.insert(name.begin() + 1, '!');
    isHalt = true;
}

void TaiConfig::TaiSection::unsetHaltPoint() {
    if (!isHalt)
        return;

    name.erase(name.begin() + 1);
    isHalt = false;
}

int TaiConfig::TaiSection::movePluginUp(int index) {
    int nidx = index-1;

    if (nidx < 0)
        return index;

    std::string tmp = pluginList[nidx];

    pluginList[nidx] = pluginList[index];
    pluginList[index] = tmp;

    return nidx;
}

int TaiConfig::TaiSection::movePluginDown(int index) {
    int nidx = index+1;

    if (nidx >= pluginList.size())
        return index;

    std::string tmp = pluginList[nidx];

    pluginList[nidx] = pluginList[index];
    pluginList[index] = tmp;

    return nidx;
}

void TaiConfig::removeSection(int index) {
    sectionList.erase(sectionList.begin() + index);

    if (!sectionList.empty())
        secListIt = sectionList.begin() + index;
}

void TaiConfig::disablePluginForAllSections(const std::string &plugin) {
    std::vector<int> emptySecList;
    int i = 0;

    for (std::shared_ptr<TaiConfig::TaiSection> tsc: sectionList) {
        tsc->removePlugin(plugin);

        if (tsc->isEmpty())
            emptySecList.push_back(i);

        ++i;
    }

    for (int idx: emptySecList)
        removeSection(idx);

    secListIt = sectionList.begin();
}

/**
 * Return a section, or create one, if not found, with the specified name.
 *
 * @param name The name of the section
 *
 * @return std::shared_ptr<TaiConfig::TaiSection>
*/
std::shared_ptr<TaiConfig::TaiSection> TaiConfig::getSection(const std::string &name) {
    std::shared_ptr<TaiConfig::TaiSection> section;

    for (std::shared_ptr<TaiConfig::TaiSection> tsc: sectionList) {
        std::string l1 = toLowerCase(tsc->getName());
        std::string l2 = toLowerCase(name);

        if (l1.compare(l2) == 0)
            return tsc;
    }

    section = std::make_shared<TaiConfig::TaiSection>(name);

    sectionList.push_back(section);
    secListIt = sectionList.begin();

    return section;
}

/**
 * Return the next section.
 * If the end is reached, it will reset the iterator and return the first item
 *
 * @return std::shared_ptr<TaiConfig::TaiSection>
 */
std::shared_ptr<TaiConfig::TaiSection> TaiConfig::getIterNextSection() {
    if (isEmpty())
        return nullptr;

    std::shared_ptr<TaiConfig::TaiSection> section;

    ++secListIt;

    if (secListIt == sectionList.end())
        secListIt = sectionList.begin();

    section = *secListIt;

    return section;
}

/**
 * Return the previous section.
 * If the start is reached, it will return the last item
 *
 * @return std::shared_ptr<TaiConfig::TaiSection>
 */
std::shared_ptr<TaiConfig::TaiSection> TaiConfig::getIterPrevSection() {
    if (isEmpty())
        return nullptr;

    std::shared_ptr<TaiConfig::TaiSection> section;

    if (secListIt == sectionList.begin())
        secListIt = sectionList.end() - 1;
    else
        --secListIt;

    section = *secListIt;

    return section;
}

std::vector<std::string> TaiConfig::getRawConfig() const {
    std::vector<std::string> raw;

    for (const std::shared_ptr<TaiConfig::TaiSection> &sec: sectionList) {
        raw.push_back(sec->getName());

        for (const std::string &plugin: sec->getPluginList())
            raw.push_back(plugin);
    }

    return raw;
}

PLGM_RET TaiConfig::parse() {
    std::ifstream config {path};
    std::shared_ptr<TaiConfig::TaiSection> curSection;
    std::string line;

    if (!config.is_open())
        return PLGM_RET::EOPEN_TAI_CONFIG;

    while (std::getline(config >> std::ws, line)) {
        switch (line.at(0)) {
            case '#': // comment
                break;
            case '*': { // section
                trimString(line);

                curSection = getSection(line);
            }
                break;
            default: { // plugin path
                curSection->addPlugin(line);
            }
                break;
        }
    }

    return PLGM_RET::SUCCESS;
}

PLGM_RET TaiConfig::write() const {
    std::ofstream configtxt {path};

    if (!configtxt.is_open())
        return PLGM_RET::EOPEN_TAI_CONFIG;

    for (std::shared_ptr<TaiConfig::TaiSection> sec: sectionList) {
        configtxt << sec->getName() << "\n";

        for (std::string plugin: sec->getPluginList())
            configtxt << plugin << "\n";
    }

    configtxt.close();

    return PLGM_RET::SUCCESS;
}

PLGM_RET TaiConfig::reload() {
    sectionList.clear();

    return parse();
}

void TaiConfig::reset() {
    sectionList.clear();

    std::shared_ptr<TaiConfig::TaiSection> main {getSection("*main")};
    std::shared_ptr<TaiConfig::TaiSection> npx15 {getSection("*NPXS10015")};
    std::shared_ptr<TaiConfig::TaiSection> npx16 {getSection("*NPXS10016")};

    main->addPlugin(henkakuPath);
    npx15->addPlugin(henkakuPath);
    npx16->addPlugin(henkakuPath);

    secListIt = sectionList.begin();
    requiresRebootToApply = true;
}