/*
	Plugins Manager
	Copyright (C) 2017-2022, kylon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <cstring>
#include <psp2/apputil.h>
#include <psp2/system_param.h>
#include <psp2/kernel/threadmgr.h>

#include "../include/VitaControls.h"

VitaControls::VitaControls() {
    SceAppUtilInitParam init_param;
    SceAppUtilBootParam boot_param;
    int ret;

    std::memset(&init_param, 0, sizeof(SceAppUtilInitParam));
    std::memset(&boot_param, 0, sizeof(SceAppUtilBootParam));
    sceAppUtilInit(&init_param, &boot_param);

    sceCtrlSetSamplingMode(SCE_CTRL_MODE_ANALOG);
    sceAppUtilSystemParamGetInt(SCE_SYSTEM_PARAM_ID_ENTER_BUTTON, &ret);

    if (ret == SCE_SYSTEM_PARAM_ENTER_BUTTON_CIRCLE) {
        enterButton = SCE_CTRL_CIRCLE;
        backButton = SCE_CTRL_CROSS;
    }

    sceAppUtilShutdown();
}

bool VitaControls::isButton(uint32_t btn) const {
    return pad.buttons & btn;
}

bool VitaControls::holdEnd(uint32_t btn) const {
    return (oldPad.buttons & btn) && !isButton(btn);
}

uint32_t VitaControls::_getKeys() {
    std::memset(&pad, 0, sizeof(pad));
    std::memset(&oldPad, 0, sizeof(oldPad));

    while (true) {
        sceCtrlPeekBufferPositive(0, &pad, 1);

        if (pad.ly < analogCenterThresholdDown) { // not setting pad
            sceKernelDelayThread(50000);
            return sceCtrlAnalogUp;

        } else if (pad.ly > analogCenterThresholdUp) { // not setting pad
            sceKernelDelayThread(50000);
            return sceCtrlAnalogDown;

        } else if (holdEnd(enterButton)) {
            sceKernelDelayThread(190000);
            return enterButton;

        } else if (holdEnd(backButton)) {
            sceKernelDelayThread(190000);
            return backButton;

        } else if (holdEnd(SCE_CTRL_SQUARE)) {
            sceKernelDelayThread(190000);
            return SCE_CTRL_SQUARE;

        } else if (holdEnd(SCE_CTRL_TRIANGLE)) {
            sceKernelDelayThread(190000);
            return SCE_CTRL_TRIANGLE;

        } else if (isButton(SCE_CTRL_UP)) {
            sceKernelDelayThread(150000);
            return SCE_CTRL_UP;

        } else if (isButton(SCE_CTRL_DOWN)) {
            sceKernelDelayThread(150000);
            return SCE_CTRL_DOWN;

        } else if (isButton(SCE_CTRL_LEFT)) {
            sceKernelDelayThread(150000);
            return SCE_CTRL_LEFT;

        } else if (isButton(SCE_CTRL_RIGHT)) {
            sceKernelDelayThread(150000);
            return SCE_CTRL_RIGHT;

        } else if (isButton(SCE_CTRL_LTRIGGER)) {
            sceKernelDelayThread(150000);
            return SCE_CTRL_LTRIGGER;

        } else if (isButton(SCE_CTRL_RTRIGGER)) {
            sceKernelDelayThread(150000);
            return SCE_CTRL_RTRIGGER;
        }

        oldPad = pad;
    }
}

void VitaControls::getKeys() {
    pressedButton = _getKeys();
}

bool VitaControls::isButtonEnter() const {
    return pressedButton == enterButton;
}

bool VitaControls::isButtonBack() const {
    return pressedButton == backButton;
}

bool VitaControls::isButtonSquare() const {
    return pressedButton == SCE_CTRL_SQUARE;
}

bool VitaControls::isButtonTriangle() const {
    return pressedButton == SCE_CTRL_TRIANGLE;
}

bool VitaControls::isButtonLTrigger() const {
    return pressedButton == SCE_CTRL_LTRIGGER;
}

bool VitaControls::isButtonRTrigger() const {
    return pressedButton == SCE_CTRL_RTRIGGER;
}

bool VitaControls::isButtonUp() const {
    return pressedButton == SCE_CTRL_UP;
}

bool VitaControls::isButtonDown() const {
    return pressedButton == SCE_CTRL_DOWN;
}

bool VitaControls::isButtonLeft() const {
    return pressedButton == SCE_CTRL_LEFT;
}

bool VitaControls::isButtonRight() const {
    return pressedButton == SCE_CTRL_RIGHT;
}

bool VitaControls::isButtonAnalogUp() const {
    return pressedButton == sceCtrlAnalogUp;
}

bool VitaControls::isButtonAnalogDown() const {
    return pressedButton == sceCtrlAnalogDown;
}