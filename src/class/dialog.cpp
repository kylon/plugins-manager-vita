/*
	Plugins Manager
	Copyright (C) 2017-2022, kylon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <cstring>
#include <psp2/apputil.h>
#include <psp2/system_param.h>

#include "../include/dialog.h"
#include "../include/VitaControls.h"
#include "../include/vita2d.h"

void Dialog::init() {
    SceAppUtilInitParam init_param;
    SceAppUtilBootParam boot_param;
    int ret;

    std::memset(&init_param, 0, sizeof(SceAppUtilInitParam));
    std::memset(&boot_param, 0, sizeof(SceAppUtilBootParam));
    sceAppUtilInit(&init_param, &boot_param);

    sceAppUtilSystemParamGetInt(SCE_SYSTEM_PARAM_ID_ENTER_BUTTON, &ret);
    sceAppUtilShutdown();

    enterButton = SCE_SYSTEM_PARAM_ENTER_BUTTON_CIRCLE ? SCE_CTRL_CIRCLE : SCE_CTRL_CROSS;
}

Dialog::DIALOG_RESULT Dialog::showMessageDialog(const char *msg, Dialog::DIALOG_BUTTON_TYPE buttonType) {
    int textW = vita2d::getInstance().getTextWidth(msg);
    int textH = vita2d::getInstance().getTextHeight(msg);
    int w = textW + 60;
    int h = textH + 70;
    int x = vita2d::getInstance().getScreenXCenter() - (w/2);
    int y = vita2d::getInstance().getScreenYCenter() - (h/2);
    char buttonsTxt[50] = {0};
    int btnTextW;

    switch (buttonType) {
        case Dialog::DIALOG_BUTTON_TYPE::BUTTONS_YESNO:
            snprintf(buttonsTxt, sizeof(buttonsTxt), "X Yes   O No");
            break;
        default:
            snprintf(buttonsTxt, sizeof(buttonsTxt), "X ok");
            break;
    }

    btnTextW = vita2d::getInstance().getTextWidth(buttonsTxt);

    vita2d::getInstance().startRenderingNoClear();
    vita2d::getInstance().setTextColor(vita2d::COLOR::WHITE);
    vita2d::getInstance().setObjectColor(vita2d::COLOR::WHITE);
    vita2d::getInstance().drawRect(x-5, y-5, w+10, h+10);
    vita2d::getInstance().setObjectColor(vita2d::COLOR::LIGHTGREY);
    vita2d::getInstance().drawRect(x, y, w, h);
    vita2d::getInstance().drawText(vita2d::getInstance().getScreenXCenter() - (textW/2), y+30, msg);
    vita2d::getInstance().drawText(vita2d::getInstance().getScreenXCenter() - (btnTextW/2), y+h-20, buttonsTxt);
    vita2d::getInstance().endRendering();

    while (true) {
        VitaControls::getInstance().getKeys();

        switch (buttonType) {
            case Dialog::DIALOG_BUTTON_TYPE::BUTTONS_YESNO: {
                if (VitaControls::getInstance().isButtonEnter())
                    return Dialog::DIALOG_RESULT::YES;
                else if (VitaControls::getInstance().isButtonBack())
                    return Dialog::DIALOG_RESULT::NO;
            }
                break;
            default: {
                if (VitaControls::getInstance().isButtonEnter())
                    return Dialog::DIALOG_RESULT::OK;
            }
                break;
        }
    }

    return Dialog::DIALOG_RESULT::ERROR;
}