/*
	Plugins Manager
	Copyright (C) 2017-2022, kylon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "../include/plgmMenu.h"
#include "../include/vita2d.h"

void PlgmMenu::toggle() {
    selected = defaultSelected;
    state = !state;
}

PlgmMenu::MENU_OPTION PlgmMenu::getSelectedOption() const {
    return (PlgmMenu::MENU_OPTION)selected;
}

void PlgmMenu::setMenuMode(PlgmMenu::MENU_MODE mode) {
    optionsFlag.fill(false);

    switch (mode) {
        case PlgmMenu::MENU_MODE::CONFIG_UI: {
            optionsFlag[PlgmMenu::MENU_OPTION::REMOVE_SECTION] = true;
            optionsFlag[PlgmMenu::MENU_OPTION::DISABLE_PLUGIN] = true;
            optionsFlag[PlgmMenu::MENU_OPTION::SET_HALT] = true;
            optionsFlag[PlgmMenu::MENU_OPTION::UNSET_HALT] = true;
            optionsFlag[PlgmMenu::MENU_OPTION::MOVE_PLUGIN_UP] = true;
            optionsFlag[PlgmMenu::MENU_OPTION::MOVE_PLUGIN_DOWN] = true;
            optionsFlag[PlgmMenu::MENU_OPTION::SAVE_CONFIG] = true;
            optionsFlag[PlgmMenu::MENU_OPTION::RELOAD_CONFIG] = true;
            optionsFlag[PlgmMenu::MENU_OPTION::RESET_CONFIG] = true;

            defaultSelected = PlgmMenu::MENU_OPTION::REMOVE_SECTION;
        }
            break;
        case PlgmMenu::MENU_MODE::OVERVIEW_UI: {
            optionsFlag[PlgmMenu::MENU_OPTION::SAVE_CONFIG] = true;
            optionsFlag[PlgmMenu::MENU_OPTION::RELOAD_CONFIG] = true;
            optionsFlag[PlgmMenu::MENU_OPTION::RESET_CONFIG] = true;

            defaultSelected = PlgmMenu::MENU_OPTION::SAVE_CONFIG;
        }
            break;
        case PlgmMenu::MENU_MODE::ENABLED_UI: {
            optionsFlag[PlgmMenu::MENU_OPTION::DISABLE_PLUGIN] = true;
            optionsFlag[PlgmMenu::MENU_OPTION::DELETE_PLUGIN] = true;
            optionsFlag[PlgmMenu::MENU_OPTION::SAVE_CONFIG] = true;
            optionsFlag[PlgmMenu::MENU_OPTION::RELOAD_CONFIG] = true;
            optionsFlag[PlgmMenu::MENU_OPTION::RESET_CONFIG] = true;

            defaultSelected = PlgmMenu::MENU_OPTION::DISABLE_PLUGIN;
        }
            break;
        case PlgmMenu::MENU_MODE::DISABLED_UI: {
            optionsFlag[PlgmMenu::MENU_OPTION::ENABLE_PLUGIN] = true;
            optionsFlag[PlgmMenu::MENU_OPTION::DELETE_PLUGIN] = true;
            optionsFlag[PlgmMenu::MENU_OPTION::SAVE_CONFIG] = true;
            optionsFlag[PlgmMenu::MENU_OPTION::RELOAD_CONFIG] = true;
            optionsFlag[PlgmMenu::MENU_OPTION::RESET_CONFIG] = true;

            defaultSelected = PlgmMenu::MENU_OPTION::ENABLE_PLUGIN;
        }
            break;
        case PlgmMenu::MENU_MODE::ALL_PLUGINS_UI: {
            optionsFlag[PlgmMenu::MENU_OPTION::ENABLE_PLUGIN] = true;
            optionsFlag[PlgmMenu::MENU_OPTION::DELETE_PLUGIN] = true;
            optionsFlag[PlgmMenu::MENU_OPTION::SAVE_CONFIG] = true;
            optionsFlag[PlgmMenu::MENU_OPTION::RELOAD_CONFIG] = true;
            optionsFlag[PlgmMenu::MENU_OPTION::RESET_CONFIG] = true;

            defaultSelected = PlgmMenu::MENU_OPTION::ENABLE_PLUGIN;
        }
            break;
        case PlgmMenu::MENU_MODE::FSBROWSER_UI: {
            optionsFlag[PlgmMenu::MENU_OPTION::ENABLE_PLUGIN] = true;
            optionsFlag[PlgmMenu::MENU_OPTION::DELETE_FILE] = true;
            optionsFlag[PlgmMenu::MENU_OPTION::SAVE_CONFIG] = true;
            optionsFlag[PlgmMenu::MENU_OPTION::RELOAD_CONFIG] = true;
            optionsFlag[PlgmMenu::MENU_OPTION::RESET_CONFIG] = true;

            defaultSelected = PlgmMenu::MENU_OPTION::ENABLE_PLUGIN;
        }
            break;
        default:
            break;
    }
}

void PlgmMenu::draw() {
    int tx = x+20;
    int ty = 40;
    int tyPad = 30;

	vita2d::getInstance().setObjectColor(vita2d::COLOR::LIGHTGREY);
    vita2d::getInstance().setTextColor(vita2d::COLOR::WHITE);
    vita2d::getInstance().drawRect(x, y, w, h);

    for (int i=0; i<PlgmMenu::optionsN; ++i) {
        if (!optionsFlag[i])
            continue;

        if (i == selected) {
            vita2d::getInstance().setTextColor(vita2d::COLOR::GREEN);
            vita2d::getInstance().drawText(tx, ty, PlgmMenu::optionsText[i]);
            vita2d::getInstance().setTextColor(vita2d::COLOR::WHITE);

        } else {
            vita2d::getInstance().drawText(tx, ty, PlgmMenu::optionsText[i]);
        }

        ty += tyPad;
    }
}
