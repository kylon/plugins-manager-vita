/*
	Plugins Manager
	Copyright (C) 2017-2022, kylon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <cstring>

#include "../include/pluginsFolder.h"
#include "../include/utils.h"
#include "../include/file.h"

PLGM_RET PluginsFolder::scanPluginsFolder() {
    SceUID dfd = sceIoDopen(path);
    int ret;

    if (dfd < 0)
		return PLGM_RET::EREAD_PLUGINS_FOLD;

    do {
		std::string name, lname;
		SceIoDirent dir;

		std::memset(&dir, 0, sizeof(SceIoDirent));

		ret = sceIoDread(dfd, &dir);
		if (ret <= 0) {
			sceIoDclose(dfd);
			return PLGM_RET::EREAD_PLUGINS_FOLD;
		}

		name = std::string(dir.d_name);
		lname = toLowerCase(name);

		if (!containsStr(lname, ".suprx") && !containsStr(lname, ".skprx"))
			continue;

		list.push_back(name);
	} while (ret > 0);

    sceIoDclose(dfd);
	return PLGM_RET::SUCCESS;
}

bool PluginsFolder::deletePlugin(const std::string &name, const std::string &pluginPath) {
	int ret = ioRemove(pluginPath.c_str());

	if (ret == 0)
		return false;

	list.erase(std::find(list.begin(), list.end(), name));
	return true;
}