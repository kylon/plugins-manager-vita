/*
	Plugins Manager
	Copyright (C) 2017-2022, kylon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <cstdint>
#include <psp2/gxm.h>

#include "../include/vita2d.h"

vita2d::vita2d() {
    vita2d_init();

    pgf = vita2d_load_default_pgf();
}

uint32_t vita2d::getColorInt(vita2d::COLOR color) const {
    switch (color) {
        case vita2d::COLOR::LIGHTRED:
            return RGBA8(222, 89, 89, 255);
        case vita2d::COLOR::LIGHTGREY:
            return RGBA8(32,37,47,255);
        case vita2d::COLOR::LIGHTBLUE:
            return RGBA8(5,80,150,255);
        case vita2d::COLOR::GREEN:
            return RGBA8(55, 195, 0, 255);
        case vita2d::COLOR::YELLOW:
            return RGBA8(165, 170, 70, 255);
        case vita2d::COLOR::WHITE:
        default:
            return RGBA8(255, 255, 255, 255);
    }
}

void vita2d::setTextColor(vita2d::COLOR color) {
    textColor = getColorInt(color);
}

void vita2d::setObjectColor(vita2d::COLOR color) {
    objectColor = getColorInt(color);
}

void vita2d::drawText(int x, int y, const std::string &text) const {
    vita2d_pgf_draw_text(pgf, x, y, textColor, 1.1f, text.c_str());
}

void vita2d::drawText(int x, int y, const char *text) const {
    vita2d_pgf_draw_text(pgf, x, y, textColor, 1.1f, text);
}

void vita2d::drawText(int x, int y, float scale, const std::string &text) const {
    vita2d_pgf_draw_text(pgf, x, y, textColor, scale, text.c_str());
}

void vita2d::drawText(int x, int y, float scale, const char *text) const {
    vita2d_pgf_draw_text(pgf, x, y, textColor, scale, text);
}

void vita2d::drawRect(int x, int y, int width, int height) const {
    vita2d_draw_rectangle(x, y, width, height, objectColor);
}

void vita2d::drawCircle(int x, int y, float rad) const {
    vita2d_draw_fill_circle(x, y, rad, objectColor);
}

int vita2d::getTextWidth(const char *text) const {
    return vita2d_pgf_text_width(pgf, 1.1f, text);
}

int vita2d::getTextWidth(const std::string &text) const {
    return vita2d_pgf_text_width(pgf, 1.1f, text.c_str());
}

int vita2d::getTextHeight(const char *text) const {
    return vita2d_pgf_text_height(pgf, 1.1f, text);
}

int vita2d::getTextHeight(const std::string &text) const {
    return vita2d_pgf_text_height(pgf, 1.1f, text.c_str());
}

void vita2d::startRendering() const {
    vita2d_start_drawing();
    vita2d_clear_screen();
}

void vita2d::startRenderingNoClear() const {
    // swap 2 times as a bad workaround to draw to current fb
    vita2d_swap_buffers();
    vita2d_swap_buffers();

    vita2d_start_drawing();
}

void vita2d::endRendering() const {
    vita2d_end_drawing();
    vita2d_wait_rendering_done();
    vita2d_swap_buffers();
}