/*
	Plugins Manager
	Copyright (C) 2017-2022, kylon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <array>
#include <malloc.h>

#include "include/file.h"

int exists(const char *path) {
    SceIoStat stat = {0};

    return sceIoGetstat(path, &stat) >= 0;
}

int isDir(const char *path) {
    SceIoStat stat = {0};

    if (sceIoGetstat(path, &stat) < 0)
        return 0;

    return SCE_S_ISDIR(stat.st_mode);
}

int copyFile(const char *src, const char *dest) {
    if (!strcasecmp(src, dest))
        return 1;
    else if (!exists(src))
        return 0;

    void *buf = memalign(4096, 128 * 1024);
    int fsrc = sceIoOpen(src, SCE_O_RDONLY, 0);
    int fdst  = sceIoOpen(dest, SCE_O_WRONLY | SCE_O_CREAT, 0777);
    int read = 0, written = 0;
    SceIoStat stat;

    if (fsrc < 0 || fdst < 0) {
        free(buf);
        sceIoClose(fsrc);
        sceIoClose(fdst);
        return 0;
    }

    while (1) {
        read = sceIoRead(fsrc, buf, 128 * 1024);
        if (read < 0) {
            free(buf);
            sceIoClose(fsrc);
            sceIoClose(fdst);
            sceIoRemove(dest);
            return 0;

        } else if (!read) {
            break;
        }

        written = sceIoWrite(fdst, buf, read);
        if (written < 0) {
            free(buf);
            sceIoClose(fsrc);
            sceIoClose(fdst);
            sceIoRemove(dest);
            return 0;
        }
    }

    free(buf);

    // Inherit file stat
    memset(&stat, 0, sizeof(SceIoStat));
    sceIoGetstatByFd(fsrc, &stat);
    sceIoChstatByFd(fdst, &stat, 0x3B);

    sceIoClose(fsrc);
    sceIoClose(fdst);

    return 1;
}

int copyDir(const char *src, const char *dest) {
    if (!strcasecmp(src, dest))
        return 1;

    SceUID dfd = sceIoDopen(src);
    SceIoStat stat;
    int res = 0, ret = 0;

    if (dfd < 0) // not a folder?
        return copyFile(src, dest);

    memset(&stat, 0, sizeof(SceIoStat));
    sceIoGetstatByFd(dfd, &stat);

    stat.st_mode |= SCE_S_IWUSR;

    ret = sceIoMkdir(dest, stat.st_mode & 0xFFF);
    if (ret < 0 && ret != SCE_ERROR_ERRNO_EEXIST) {
        sceIoDclose(dfd);
        return 0;
    }

    if (ret == SCE_ERROR_ERRNO_EEXIST)
        sceIoChstat(dest, &stat, 0x3B);

    do {
        SceIoDirent dir;

        memset(&dir, 0, sizeof(SceIoDirent));

        res = sceIoDread(dfd, &dir);
        if (res <= 0 || strcmp(dir.d_name, ".") == 0 || strcmp(dir.d_name, "..") == 0)
            continue;

        int dPathLen = strlen(dir.d_name);
        int srcLen = strlen(src) + dPathLen + 4;
        int dstLen = strlen(dest) + dPathLen + 4;
        char *new_src = (char *) malloc(srcLen);
        char *new_dst = (char *) malloc(dstLen);

        snprintf(new_src, srcLen, "%s/%s", src, dir.d_name);
        snprintf(new_dst, dstLen, "%s/%s", dest, dir.d_name);

        if (SCE_S_ISDIR(dir.d_stat.st_mode))
            ret = copyDir(new_src, new_dst);
        else
            ret = copyFile(new_src, new_dst);

        free(new_src);
        free(new_dst);

        if (!ret) {
            sceIoDclose(dfd);
            return 0;
        }
    } while (res > 0);

    sceIoDclose(dfd);

    return 1;
}

int ioRemove(const char *path) {
	if (!exists(path))
		return 1;

	SceUID dfd = sceIoDopen(path);
	int res = 0;

	if (dfd < 0) { // not a folder, file?
		if (sceIoRemove(path) >= 0) // file
			return 1;

		return 0;
	}

	do {
		SceIoDirent dir;
		char *newPath = NULL;
		size_t pLen = 0;
		int ret;

		memset(&dir, 0, sizeof(SceIoDirent));

		res = sceIoDread(dfd, &dir);
		if (res <= 0 || strcmp(dir.d_name, ".") == 0 || strcmp(dir.d_name, "..") == 0)
			continue;

		pLen = sizeof(char) * (strlen(path) + strlen(dir.d_name) + 2);
		newPath = (char *) malloc(pLen);

		snprintf(newPath, pLen, "%s/%s", path, dir.d_name);

		if (SCE_S_ISDIR(dir.d_stat.st_mode))
			ret = ioRemove(newPath) == 0 ? -1:1;
		else
			ret = sceIoRemove(newPath);

		free(newPath);
		if (ret < 0) {
			sceIoDclose(dfd);
			return 0;
		}
	} while (res > 0);

	sceIoDclose(dfd);

	if (sceIoRmdir(path) < 0)
		return 0;

	return 1;
}

void readDirectory(const char *path, std::vector<std::string> &out) {
    if (!isDir(path))
        return;

    SceUID dfd = sceIoDopen(path);
    int res = 0;

    if (dfd < 0)
        return;

    do {
		SceIoDirent dir;

		memset(&dir, 0, sizeof(SceIoDirent));

		res = sceIoDread(dfd, &dir);
		if (res <= 0 || strcmp(dir.d_name, ".") == 0 || strcmp(dir.d_name, "..") == 0)
			continue;

        std::string dname = dir.d_name;
        std::string tmp = std::string(path) + "/" + dname;

        out.push_back(isDir(tmp.c_str()) ? dname + "/" : dname);
	} while (res > 0);

    sceIoDclose(dfd);

    std::sort(out.begin(), out.end(), [](const std::string &a, const std::string &b) {
        bool aFold = a.back() == '/';
        bool bFold = b.back() == '/';

        return (aFold && !bFold);
    });

    out.insert(out.begin(), "..");
}

std::vector<std::string> getAvailableMountPoints() {
    static std::array<std::string, 5> devs = {"ur0:", "uma0:", "imc0:", "xmc0:", "ux0:"};
    std::vector<std::string> ret;

    for (const std::string &mp: devs) {
        if (exists(mp.c_str()))
            ret.push_back(mp);
    }

    return ret;
}