/*
	Plugins Manager
	Copyright (C) 2017-2022, kylon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <cstddef>
#include <iterator>
#include <string>
#include <vector>
#include <memory>

#include "enums.h"

class TaiConfig {
public:
    class TaiSection {
    private:
        std::vector<std::string> pluginList;
        std::string name;
        bool isHalt;

    public:
        TaiSection(const std::string &name);

        void removePlugin(int index) {
            pluginList.erase(pluginList.begin() + index);
        }

        bool isEmpty() const { return pluginList.empty(); }
        const std::vector<std::string> &getPluginList() const { return pluginList; }
        const std::string &getName() const { return name; }
        int getPluginsCount() const { return pluginList.size(); }

        void addPlugin(const std::string &plugin);
        void removePlugin(const std::string &plugin);
        void setHaltPoint();
        void unsetHaltPoint();
        int movePluginUp(int index);
        int movePluginDown(int index);
    };

    bool requiresRebootToApply {false};

    const std::vector<std::shared_ptr<TaiConfig::TaiSection>> &getSectionList() const { return sectionList; }
    std::shared_ptr<TaiConfig::TaiSection> getIterSection() const { return isEmpty() ? nullptr : *secListIt; }
    int getIterSectionIndex() const { return secListIt - sectionList.begin(); }
    bool isEmpty() const { return sectionList.empty(); }

    void removeSection(int index);
    void disablePluginForAllSections(const std::string &plugin);
    std::shared_ptr<TaiConfig::TaiSection> getSection(const std::string &);
    std::shared_ptr<TaiConfig::TaiSection> getIterNextSection();
    std::shared_ptr<TaiConfig::TaiSection> getIterPrevSection();
    std::vector<std::string> getRawConfig() const;
    PLGM_RET parse();
    PLGM_RET write() const;
    PLGM_RET reload();
    void reset();

private:
    std::string path {"ur0:tai/config.txt"};
    std::string henkakuPath {"ur0:tai/henkaku.suprx"};
    std::vector<std::shared_ptr<TaiConfig::TaiSection>> sectionList;
    std::vector<std::shared_ptr<TaiConfig::TaiSection>>::iterator secListIt;
};