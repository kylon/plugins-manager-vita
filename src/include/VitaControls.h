/*
	Plugins Manager
	Copyright (C) 2017-2022, kylon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <cstdint>
#include <psp2/ctrl.h>

class VitaControls {
private:
    static const uint32_t sceCtrlAnalogUp {0x02000000};
    static const uint32_t sceCtrlAnalogDown {0x08000000};
    static const unsigned char analogCenterThresholdUp {192}; // CENTER 128 + THRESHOLD 64
    static const unsigned char analogCenterThresholdDown {64}; // CENTER 128 - THRESHOLD 64
    uint32_t enterButton {SCE_CTRL_CROSS};
    uint32_t backButton {SCE_CTRL_CIRCLE};
    uint32_t pressedButton;
    SceCtrlData oldPad;
    SceCtrlData pad;

    VitaControls();

    bool holdEnd(uint32_t btn) const;
    bool isButton(uint32_t btn) const;
    uint32_t _getKeys();

public:
    static VitaControls &getInstance() {
        static VitaControls instance;
        return instance;
    }

    VitaControls(VitaControls const &) = delete;
    void operator=(VitaControls const &) = delete;

    void getKeys();
    bool isButtonEnter() const;
    bool isButtonBack() const;
    bool isButtonTriangle() const;
    bool isButtonSquare() const;
    bool isButtonLTrigger() const;
    bool isButtonRTrigger() const;
    bool isButtonUp() const;
    bool isButtonDown() const;
    bool isButtonLeft() const;
    bool isButtonRight() const;
    bool isButtonAnalogUp() const;
    bool isButtonAnalogDown() const;
};