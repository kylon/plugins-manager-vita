/*
	Plugins Manager
	Copyright (C) 2017-2022, kylon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <cstdint>
#include <psp2/ctrl.h>

class Dialog {
private:
    inline static uint32_t enterButton {SCE_CTRL_CROSS};

    Dialog() {};

public:
    enum DIALOG_BUTTON_TYPE {
        BUTTONS_YESNO,
        BUTTONS_OK
    };

    enum DIALOG_RESULT {
        YES,
        NO,
        OK,
        ERROR
    };

    Dialog(Dialog const &) = delete;
    void operator=(Dialog const &) = delete;

    static void init();
    static DIALOG_RESULT showMessageDialog(const char *, Dialog::DIALOG_BUTTON_TYPE);
};