/*
	Plugins Manager
	Copyright (C) 2017-2022, kylon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

enum PLGM_RET {
    SUCCESS = 0,
    EOPEN_TAI_CONFIG,
    ESHOW_SCEDIALOG,
    ESCEDIALOG_RUNNING,
    EREAD_PLUGINS_FOLD,
    EINIT_SQLITE
};

enum class PLGM_STATE: int {
    BEGIN = 0,
    CONFIG_UI,
    OVERVIEW_UI,
    ENABLED_UI,
    DISABLED_UI,
    ALL_PLUGINS_UI,
    FSBROWSER_UI,
    END
};