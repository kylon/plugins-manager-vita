/*
	Plugins Manager
	Copyright (C) 2017-2022, kylon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <string>
#include <vector>
#include <psp2/io/dirent.h>
#include <psp2/io/fcntl.h>
#include <psp2/io/stat.h>

#define SCE_ERROR_ERRNO_EEXIST 0x80010011

#ifdef  __cplusplus
extern "C" {
#endif

int exists(const char *path);
int isDir(const char *path);
int copyFile(const char *src, const char *dest);
int copyDir(const char *src, const char *dest);
int ioRemove(const char *path);

#ifdef __cplusplus
}
#endif

void readDirectory(const char *path, std::vector<std::string> &out);
std::vector<std::string> getAvailableMountPoints();