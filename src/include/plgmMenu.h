/*
	Plugins Manager
	Copyright (C) 2017-2022, kylon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <array>

class PlgmMenu {
private:
    static constexpr int optionsN = 12;
    static constexpr const char * const optionsText[] {
            "Remove section",
            "Enable plugin",
            "Disable plugin",
            "Delete plugin",
            "Set halt point",
            "Unset halt point",
            "Move plugin up",
            "Move plugin down",
            "Save changes",
            "Reload config file",
            "Reset to defaults",
            "Delete file/folder"
    };
    std::array<bool, PlgmMenu::optionsN> optionsFlag;
    bool state = false;
    int selected = 0;
    int x = 720;
    int y = 0;
    int w = 240;
    int h = 544;
    int defaultSelected;

public:
    enum MENU_OPTION: int {
        REMOVE_SECTION = 0,
        ENABLE_PLUGIN,
        DISABLE_PLUGIN,
        DELETE_PLUGIN,
        SET_HALT,
        UNSET_HALT,
        MOVE_PLUGIN_UP,
        MOVE_PLUGIN_DOWN,
        SAVE_CONFIG,
        RELOAD_CONFIG,
        RESET_CONFIG,
        DELETE_FILE
    };

    enum MENU_MODE {
        CONFIG_UI,
        OVERVIEW_UI,
        ENABLED_UI,
        DISABLED_UI,
        ALL_PLUGINS_UI,
        FSBROWSER_UI
    };

	bool isOpen() const { return state; }

    void toggle();
    void draw();
    void setMenuMode(MENU_MODE mode);
    PlgmMenu::MENU_OPTION getSelectedOption() const;

	PlgmMenu &operator++() {
        if ((selected+1) >= PlgmMenu::optionsN)
            return *this;

        int i = selected+1;

        while (i < PlgmMenu::optionsN && !optionsFlag[i])
            ++i;

        selected = i >= PlgmMenu::optionsN ? selected : i;

		return *this;
	}

	PlgmMenu &operator--() {
        if ((selected-1) < defaultSelected)
            return *this;

        --selected;

        while (selected > defaultSelected && !optionsFlag[selected])
            --selected;

		return *this;
	}
};
