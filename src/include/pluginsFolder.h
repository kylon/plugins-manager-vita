/*
	Plugins Manager
	Copyright (C) 2017-2022, kylon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <vector>
#include <string>

#include "enums.h"

class PluginsFolder {
private:
    static constexpr char path[] {"ur0:tai/plugins/"};

public:
   inline static std::vector<std::string> list;

    static PLGM_RET scanPluginsFolder();
    static std::string getFullPluginPath(const std::string &plugin) { return path + plugin; }
    static bool deletePlugin(const std::string &name, const std::string &pluginPath);
};