/*
	Plugins Manager
	Copyright (C) 2017-2022, kylon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <algorithm>

#include "TaiConfig.h"
#include "dialog.h"
#include "enums.h"

void trimString(std::string &str);
void trimNewLinesString(std::string &str);
std::string joinPathArr(const std::vector<std::string> &vec);
std::string toLowerCase(const std::string &str);
bool containsStr(const std::string &str, const char *find);
bool containsStr(const std::string &str, const std::string &find);
PLGM_STATE switchToPrevPlgmState(PLGM_STATE cstate);
PLGM_STATE switchToNextPlgmState(PLGM_STATE cstate);
std::vector<std::string> getEnabledPluginsList(const TaiConfig &conf);
std::vector<std::string> getDisabledPluginsList(const TaiConfig &conf);
int getNewStartIndexForList(int idx, int start, int end);
void loadApplicationsList(std::vector<std::pair<std::string, std::string>> &out);
bool initSqlite();
bool closeSqlite();
void saveConfig(TaiConfig &conf);
bool reloadConfig(TaiConfig &conf);
bool resetConfig(TaiConfig &conf);
std::string errorString(PLGM_RET err);