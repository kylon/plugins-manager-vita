/*
	Plugins Manager
	Copyright (C) 2017-2022, kylon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <cstdint>
#include <string>
#include <vita2d.h>

class vita2d {
public:
    enum COLOR {
        LIGHTRED,
        LIGHTGREY,
        LIGHTBLUE,
        WHITE,
        GREEN,
        YELLOW
    };

    static vita2d &getInstance() {
        static vita2d instance;
        return instance;
    }

    void startRendering() const;
    void startRenderingNoClear() const;
    void endRendering() const;
    void setTextColor(vita2d::COLOR);
    void setObjectColor(vita2d::COLOR);
    void drawText(int x, int y, const std::string &text) const;
    void drawText(int x, int y, const char *text) const;
    void drawText(int x, int y, float scale, const std::string &text) const;
    void drawText(int x, int y, float scale, const char *text) const;
    void drawRect(int x, int y, int width, int height) const;
    void drawCircle(int x, int y, float rad) const;
    int getTextWidth(const char *) const;
    int getTextWidth(const std::string &) const;
    int getTextHeight(const char *) const;
    int getTextHeight(const std::string &) const;

    int getScreenXCenter() const { return screenXCenter; }
    int getScreenYCenter() const { return screenYCenter; }

    vita2d(vita2d const &) = delete;
    void operator=(vita2d const &) = delete;

    ~vita2d() {
        vita2d_wait_rendering_done();
        vita2d_free_pgf(pgf);
        vita2d_fini();
    }

private:
    static constexpr int screenXCenter = 960 / 2;
    static constexpr int screenYCenter = 544 / 2;
    vita2d_pgf *pgf = nullptr;
    uint32_t textColor;
    uint32_t objectColor;

    vita2d();
    uint32_t getColorInt(vita2d::COLOR) const;
};
