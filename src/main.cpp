/*
	Plugins Manager
	Copyright (C) 2017-2022, kylon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <psp2/kernel/processmgr.h>
#include <psp2/power.h>

#include "include/TaiConfig.h"
#include "include/dialog.h"
#include "include/enums.h"
#include "include/file.h"
#include "include/utils.h"
#include "include/VitaControls.h"
#include "include/plgmMenu.h"
#include "include/pluginsFolder.h"
#include "include/vita2d.h"

void selectIDsUI(const std::vector<std::pair<std::string, std::string>> &, std::vector<int> &);

void printFatalError(PLGM_RET error) {
    std::string msg = errorString(error);
    const char *msg2 = "Press any key to exit.";
    int textW = vita2d::getInstance().getTextWidth(msg.c_str());
    int text2W = vita2d::getInstance().getTextWidth(msg2);

    vita2d::getInstance().startRendering();
    vita2d::getInstance().setTextColor(vita2d::COLOR::LIGHTRED);
    vita2d::getInstance().drawText(vita2d::getInstance().getScreenXCenter() - (textW/2), 20, msg.c_str());
    vita2d::getInstance().drawText(vita2d::getInstance().getScreenXCenter() - (text2W/2), 40, msg2);

    vita2d::getInstance().endRendering();
    VitaControls::getInstance().getKeys();
    sceKernelExitProcess(0);
}

void drawModeHead(const std::string &mode) {
    std::string modeHead = "[L] < " + mode + " > [R]";
    int textW = vita2d::getInstance().getTextWidth(modeHead.c_str());

    vita2d::getInstance().setTextColor(vita2d::COLOR::GREEN);
    vita2d::getInstance().drawText(vita2d::getInstance().getScreenXCenter() - (textW/2), 20, modeHead.c_str());
}

void handleConfigUIMenuInput(PlgmMenu::MENU_OPTION option, TaiConfig &conf,
                             std::shared_ptr<TaiConfig::TaiSection> &section,
                             int &idx, int &start) {

    switch (option) {
        case PlgmMenu::MENU_OPTION::DISABLE_PLUGIN: {
            if (section == nullptr || section->isEmpty())
                break;

            section->removePlugin(idx);

            if (idx >= section->getPluginsCount())
                idx = section->getPluginsCount() - 1;

            conf.requiresRebootToApply = toLowerCase(section->getName()) == "*kernel";
        }
            break;
        case PlgmMenu::MENU_OPTION::REMOVE_SECTION: {
            if (conf.isEmpty())
                break;

            conf.requiresRebootToApply = toLowerCase(section->getName()) == "*kernel";

            conf.removeSection(conf.getIterSectionIndex());
            section = conf.getIterSection();
            idx = start = 0;
        }
            break;
        case PlgmMenu::MENU_OPTION::SET_HALT: {
            if (conf.isEmpty())
                break;

            section->setHaltPoint();
        }
            break;
        case PlgmMenu::MENU_OPTION::UNSET_HALT: {
            if (conf.isEmpty())
                break;

            section->unsetHaltPoint();
        }
            break;
        case PlgmMenu::MENU_OPTION::MOVE_PLUGIN_UP: {
            if (section == nullptr || section->isEmpty())
                return;

            idx = section->movePluginUp(idx);
        }
            break;
        case PlgmMenu::MENU_OPTION::MOVE_PLUGIN_DOWN: {
            if (section == nullptr || section->isEmpty())
                return;

            idx = section->movePluginDown(idx);
        }
            break;
        case PlgmMenu::MENU_OPTION::SAVE_CONFIG: {
            saveConfig(conf);
        }
            break;
        case PlgmMenu::MENU_OPTION::RELOAD_CONFIG: {
            bool ret = reloadConfig(conf);

            if (!ret)
                break;

            idx = start = 0;
            section = conf.getIterSection();
        }
            break;
        case PlgmMenu::MENU_OPTION::RESET_CONFIG: {
            bool ret = resetConfig(conf);

            if (!ret)
                break;

            idx = start = 0;
            section = conf.getIterSection();
        }
            break;
        default:
            break;
    }
}

void handleOverviewUIMenuInput(PlgmMenu::MENU_OPTION option, TaiConfig &conf, std::vector<std::string> &list,
                                int &idx, int &start) {
    switch (option) {
        case PlgmMenu::MENU_OPTION::SAVE_CONFIG:
            saveConfig(conf);
            break;
        case PlgmMenu::MENU_OPTION::RELOAD_CONFIG: {
            reloadConfig(conf);

            list = conf.getRawConfig();
            idx = start = 0;
        }
            break;
        case PlgmMenu::MENU_OPTION::RESET_CONFIG: {
            resetConfig(conf);

            list = conf.getRawConfig();
            idx = start = 0;
        }
            break;
        default:
            break;
    }
}

void handleEnabledDisabledAllUIMenuInput(PlgmMenu::MENU_MODE menuMode, PlgmMenu::MENU_OPTION option,
                                TaiConfig &conf, std::vector<std::string> &list, int &idx, int &start,
                                const std::vector<std::pair<std::string, std::string>> &appsList) {
    switch (option) {
        case PlgmMenu::MENU_OPTION::DISABLE_PLUGIN: {
            Dialog::DIALOG_RESULT ret = Dialog::showMessageDialog(
                                            "You will disable this plugin for all sections.\n\n" \
                                            "In addition, empty sections will be removed.\n\n" \
                                            "Are you sure?",
                                            Dialog::DIALOG_BUTTON_TYPE::BUTTONS_YESNO);

            if (ret != Dialog::DIALOG_RESULT::YES)
                break;

            conf.requiresRebootToApply = containsStr(list[idx], ".skprx");
            conf.disablePluginForAllSections(PluginsFolder::getFullPluginPath(list[idx]));
            list.erase(std::find(list.begin(), list.end(), list[idx]));

            if (idx >= list.size())
                --idx;
        }
            break;
        case PlgmMenu::MENU_OPTION::ENABLE_PLUGIN: {
            std::vector<int> selected;

            selectIDsUI(appsList, selected);

            if (!selected.size())
                break;

            for (int i: selected) {
                std::shared_ptr<TaiConfig::TaiSection> sec = conf.getSection(appsList[i].first);

                sec->addPlugin(PluginsFolder::getFullPluginPath(list[idx]));
            }

            conf.requiresRebootToApply = containsStr(list[idx], ".skprx");

            if (menuMode == PlgmMenu::MENU_MODE::DISABLED_UI)
                list.erase(std::find(list.begin(), list.end(), list[idx]));
        }
            break;
        case PlgmMenu::MENU_OPTION::DELETE_PLUGIN: {
            Dialog::DIALOG_RESULT ret = Dialog::showMessageDialog(
                                            "You will delete this plugin from your device.\n\n" \
                                            "In addition, the plugin will be disabled for all sections.\n\n" \
                                            "Are you sure?",
                                            Dialog::DIALOG_BUTTON_TYPE::BUTTONS_YESNO);

            if (ret != Dialog::DIALOG_RESULT::YES)
                break;

            std::string plugin = PluginsFolder::getFullPluginPath(list[idx]);

            if (!PluginsFolder::deletePlugin(list[idx], plugin)) {
                Dialog::showMessageDialog("I/O Error", Dialog::DIALOG_BUTTON_TYPE::BUTTONS_OK);
                break;
            }

            if (menuMode != PlgmMenu::MENU_MODE::DISABLED_UI) // yeah, *All* is not always enabled, but i'm lazy
                conf.requiresRebootToApply = containsStr(list[idx], ".skprx");

            conf.disablePluginForAllSections(plugin);
            list.erase(std::find(list.begin(), list.end(), list[idx]));

            if (idx >= list.size())
                --idx;
        }
            break;
        case PlgmMenu::MENU_OPTION::SAVE_CONFIG:
            saveConfig(conf);
            break;
        case PlgmMenu::MENU_OPTION::RELOAD_CONFIG:
            reloadConfig(conf);

            idx = start = 0;
            break;
        case PlgmMenu::MENU_OPTION::RESET_CONFIG:
            resetConfig(conf);

            idx = start = 0;
            break;
        default:
            break;
    }
}

void handleFsBrowserMenuInput(TaiConfig &conf, PlgmMenu::MENU_OPTION option, std::vector<std::string> &list,
                                int &idx, int &start, const std::string &curPath,
                                const std::vector<std::pair<std::string, std::string>> &appsList) {
    switch (option) {
        case PlgmMenu::MENU_OPTION::ENABLE_PLUGIN: {
            std::string file = curPath + list[idx];

            if (!containsStr(file, ".suprx") && !containsStr(file, ".skprx")) {
                Dialog::showMessageDialog("This is not a plugin.\n\n" \
                                        "Please select a .suprx or .skprx file.",
                                        Dialog::DIALOG_BUTTON_TYPE::BUTTONS_OK);
                break;
            }

            std::vector<int> selected;

            selectIDsUI(appsList, selected);

            if (!selected.size())
                break;

            for (int i: selected) {
                std::shared_ptr<TaiConfig::TaiSection> sec = conf.getSection(appsList[i].first);

                sec->addPlugin(file);
            }

            conf.requiresRebootToApply = containsStr(list[idx], ".skprx");
        }
            break;
        case PlgmMenu::MENU_OPTION::DELETE_FILE: {
            Dialog::showMessageDialog(
                        "If you are deleting one or more plugins,\n\n" \
                        "make sure to disable them before this operation.",
                        Dialog::DIALOG_BUTTON_TYPE::BUTTONS_OK);

            std::string path = curPath + list[idx];
            Dialog::DIALOG_RESULT dret = Dialog::showMessageDialog("This file/folder will be deleted!\n\nAre you sure?",
                                            Dialog::DIALOG_BUTTON_TYPE::BUTTONS_YESNO);
            int ret;

            if (dret == Dialog::DIALOG_RESULT::NO)
                break;

            ret = ioRemove(path.c_str());
            if (ret != 1) {
                Dialog::showMessageDialog("Error: Unable to delete this file/folder.", Dialog::DIALOG_BUTTON_TYPE::BUTTONS_OK);
                break;
            }

            list.erase(list.begin() + idx);
        }
            break;
        case PlgmMenu::MENU_OPTION::SAVE_CONFIG:
            saveConfig(conf);
            break;
        case PlgmMenu::MENU_OPTION::RELOAD_CONFIG:
            reloadConfig(conf);
            break;
        case PlgmMenu::MENU_OPTION::RESET_CONFIG:
            resetConfig(conf);
            break;
        default:
            break;
    }
}

void configUI(TaiConfig &conf, PlgmMenu &menu, PLGM_STATE &state) {
    std::shared_ptr<TaiConfig::TaiSection> taiSec = conf.getIterSection();
    float x = 10;
    float yPad = 25;
    int start = 0;
    int idx = 0;

    menu.setMenuMode(PlgmMenu::MENU_MODE::CONFIG_UI);

    while (true) {//todo test empty
        std::vector<std::string> plgList = taiSec == nullptr ? std::vector<std::string>() : taiSec->getPluginList();
        int end = start + 14;
        float y = 145;

        vita2d::getInstance().startRendering();
        drawModeHead("TaiHen Config");

        if (taiSec == nullptr) {
            vita2d::getInstance().setTextColor(vita2d::COLOR::WHITE);
            vita2d::getInstance().drawText(x, 80, "This TaiHen config is empty!");

        } else {
            vita2d::getInstance().setTextColor(vita2d::COLOR::LIGHTRED);
            vita2d::getInstance().drawText(x, 80, "< " + taiSec->getName() + " >");
            vita2d::getInstance().setTextColor(vita2d::COLOR::WHITE);
        }

        if (start > 0)
            vita2d::getInstance().drawText(x, y-yPad, "+");

        for (int i=0,l=plgList.size(); i<l && i<end; ++i) {
            if (i == idx) {
                vita2d::getInstance().setTextColor(vita2d::COLOR::GREEN);
                vita2d::getInstance().drawText(x, y, plgList[i]);
                vita2d::getInstance().setTextColor(vita2d::COLOR::WHITE);

            } else {
                vita2d::getInstance().drawText(x, y, plgList[i]);
            }

            y += yPad;
        }

        if (end < plgList.size())
            vita2d::getInstance().drawText(x, y, "+");

        if (menu.isOpen())
            menu.draw();

        vita2d::getInstance().endRendering();
        VitaControls::getInstance().getKeys();

        if (VitaControls::getInstance().isButtonTriangle()) {
            menu.toggle();

        } else if (menu.isOpen()) {
            if (VitaControls::getInstance().isButtonDown() || VitaControls::getInstance().isButtonAnalogDown())
                ++menu;
            else if (VitaControls::getInstance().isButtonUp() || VitaControls::getInstance().isButtonAnalogUp())
                --menu;
            else if (VitaControls::getInstance().isButtonEnter())
                handleConfigUIMenuInput(menu.getSelectedOption(), conf, taiSec, idx, start);

        } else {
            if (VitaControls::getInstance().isButtonLTrigger()) {
                state = switchToPrevPlgmState(state);
                break;

            } else if (VitaControls::getInstance().isButtonRTrigger()) {
                state = switchToNextPlgmState(state);
                break;

            } else if (VitaControls::getInstance().isButtonUp() || VitaControls::getInstance().isButtonAnalogUp())
                idx = (idx-1) <= 0 ? 0 : idx-1;
            else if (VitaControls::getInstance().isButtonDown() || VitaControls::getInstance().isButtonAnalogDown())
                idx = (idx+1) >= plgList.size() ? idx : idx+1;
            else if (VitaControls::getInstance().isButtonRight())
                taiSec = conf.getIterNextSection();
            else if (VitaControls::getInstance().isButtonLeft())
                taiSec = conf.getIterPrevSection();
        }

        start = getNewStartIndexForList(idx, start, end);
    }
}

void overviewUI(TaiConfig &conf, PlgmMenu &menu, PLGM_STATE &state) {
    std::vector<std::string> rawConfig = conf.getRawConfig();
    float x = 10;
    float yPad = 25;
    float pluginX = 40;
    int start = 0;
    int idx = 0;

    menu.setMenuMode(PlgmMenu::MENU_MODE::OVERVIEW_UI);

    while (true) {
        int end = start + 18;
        float y = 80;

        vita2d::getInstance().startRendering();
        drawModeHead("Config Overview");
        vita2d::getInstance().setTextColor(vita2d::COLOR::WHITE);

        if (start > 0)
            vita2d::getInstance().drawText(x, y-yPad, "+");

        for (int i=start,l=rawConfig.size(); i<l && i<end; ++i) {
            float cx = (rawConfig[i]).at(0) == '*' ? x : pluginX;

            if (i == idx) {
                vita2d::getInstance().setTextColor(vita2d::COLOR::GREEN);
                vita2d::getInstance().drawText(cx, y, rawConfig[i]);
                vita2d::getInstance().setTextColor(vita2d::COLOR::WHITE);

            } else {
                vita2d::getInstance().drawText(cx, y, rawConfig[i]);
            }

            y += yPad;
        }

        if (end < rawConfig.size())
            vita2d::getInstance().drawText(x, y, "+");

        if (menu.isOpen())
            menu.draw();

        vita2d::getInstance().endRendering();
        VitaControls::getInstance().getKeys();

        if (VitaControls::getInstance().isButtonTriangle()) {
            menu.toggle();

        } else if (menu.isOpen()) {
            if (VitaControls::getInstance().isButtonDown() || VitaControls::getInstance().isButtonAnalogDown())
                ++menu;
            else if (VitaControls::getInstance().isButtonUp() || VitaControls::getInstance().isButtonAnalogUp())
                --menu;
            else if (VitaControls::getInstance().isButtonEnter())
                handleOverviewUIMenuInput(menu.getSelectedOption(), conf, rawConfig, idx, start);

        } else {
            if (VitaControls::getInstance().isButtonLTrigger()) {
                state = switchToPrevPlgmState(state);
                break;

            } else if (VitaControls::getInstance().isButtonRTrigger()) {
                state = switchToNextPlgmState(state);
                break;

            }
            else if (VitaControls::getInstance().isButtonUp() || VitaControls::getInstance().isButtonAnalogUp())
                idx = (idx-1) <= 0 ? 0 : idx-1;
            else if (VitaControls::getInstance().isButtonDown() || VitaControls::getInstance().isButtonAnalogDown())
                idx = (idx+1) >= rawConfig.size() ? idx : idx+1;
        }

        start = getNewStartIndexForList(idx, start, end);
    }
}

void enabledDisabledAllCommUI(TaiConfig &conf, PlgmMenu &menu, PlgmMenu::MENU_MODE menuMode,
                         PLGM_STATE &state, std::vector<std::string> &list, const std::string &title,
                         const std::vector<std::pair<std::string, std::string>> &appsList) {
    float x = 10;
    float yPad = 25;
    int start = 0;
    int idx = 0;

    menu.setMenuMode(menuMode);

    while (true) {
        int end = start + 18;
        float y = 80;

        vita2d::getInstance().startRendering();
        drawModeHead(title);
        vita2d::getInstance().setTextColor(vita2d::COLOR::WHITE);

        if (start > 0)
            vita2d::getInstance().drawText(x, y-yPad, "+");

        for (int i=start,l=list.size(); i<l && i<end; ++i) {
            if (i == idx) {
                vita2d::getInstance().setTextColor(vita2d::COLOR::GREEN);
                vita2d::getInstance().drawText(x, y, list[i]);
                vita2d::getInstance().setTextColor(vita2d::COLOR::WHITE);

            } else {
                vita2d::getInstance().drawText(x, y, list[i]);
            }

            y += yPad;
        }

        if (end < list.size())
            vita2d::getInstance().drawText(x, y, "+");

        if (menu.isOpen())
            menu.draw();

        vita2d::getInstance().endRendering();
        VitaControls::getInstance().getKeys();

        if (VitaControls::getInstance().isButtonTriangle()) {
            menu.toggle();

        } else if (menu.isOpen()) {
            if (VitaControls::getInstance().isButtonDown() || VitaControls::getInstance().isButtonAnalogDown())
                ++menu;
            else if (VitaControls::getInstance().isButtonUp() || VitaControls::getInstance().isButtonAnalogUp())
                --menu;
            else if (VitaControls::getInstance().isButtonEnter())
                handleEnabledDisabledAllUIMenuInput(menuMode, menu.getSelectedOption(), conf, list, idx, start, appsList);

        } else {
            if (VitaControls::getInstance().isButtonLTrigger()) {
                state = switchToPrevPlgmState(state);
                break;

            } else if (VitaControls::getInstance().isButtonRTrigger()) {
                state = switchToNextPlgmState(state);
                break;

            }
            else if (VitaControls::getInstance().isButtonUp() || VitaControls::getInstance().isButtonAnalogUp())
                idx = (idx-1) <= 0 ? 0 : idx-1;
            else if (VitaControls::getInstance().isButtonDown() || VitaControls::getInstance().isButtonAnalogDown())
                idx = (idx+1) >= list.size() ? idx : idx+1;
        }

        start = getNewStartIndexForList(idx, start, end);
    }
}

void fileBrowserUI(TaiConfig &conf, PlgmMenu &menu, PLGM_STATE &state,
                    const std::vector<std::pair<std::string, std::string>> &appsList) {

    float x = 10;
    float yPad = 25;
    int start = 0;
    int idx = 0;
    std::vector<std::string> list = getAvailableMountPoints();
    std::vector<std::string> path;
    std::string cpath;

    menu.setMenuMode(PlgmMenu::MENU_MODE::FSBROWSER_UI);

    while (true) {
        int end = start + 16;
        float y = 120;

        vita2d::getInstance().startRendering();
        drawModeHead("File Browser");
        vita2d::getInstance().setTextColor(vita2d::COLOR::WHITE);
        vita2d::getInstance().drawText(x, y-60, cpath == "" ? "/" : cpath.c_str());

        if (start > 0)
            vita2d::getInstance().drawText(x, y-yPad, "+");

        for (int i=start,l=list.size(); i<l && i<end; ++i) {
            if (i == idx) {
                vita2d::getInstance().setTextColor(vita2d::COLOR::GREEN);
                vita2d::getInstance().drawText(x, y, list[i]);
                vita2d::getInstance().setTextColor(vita2d::COLOR::WHITE);

            } else if (list[i].back() == '/') {
                vita2d::getInstance().setTextColor(vita2d::COLOR::YELLOW);
                vita2d::getInstance().drawText(x, y, list[i]);
                vita2d::getInstance().setTextColor(vita2d::COLOR::WHITE);

            } else {
                vita2d::getInstance().drawText(x, y, list[i]);
            }

            y += yPad;
        }

        if (end < list.size())
            vita2d::getInstance().drawText(x, y, "+");

        if (menu.isOpen())
            menu.draw();

        vita2d::getInstance().endRendering();
        VitaControls::getInstance().getKeys();

        if (VitaControls::getInstance().isButtonTriangle()) {
            if (cpath != "" && list[idx] != "..")
                menu.toggle();

        } else if (menu.isOpen()) {
            if (VitaControls::getInstance().isButtonDown() || VitaControls::getInstance().isButtonAnalogDown())
                ++menu;
            else if (VitaControls::getInstance().isButtonUp() || VitaControls::getInstance().isButtonAnalogUp())
                --menu;
            else if (VitaControls::getInstance().isButtonEnter())
                handleFsBrowserMenuInput(conf, menu.getSelectedOption(), list, idx, start, cpath, appsList);

        } else {
            if (VitaControls::getInstance().isButtonLTrigger()) {
                state = switchToPrevPlgmState(state);
                break;

            } else if (VitaControls::getInstance().isButtonRTrigger()) {
                state = switchToNextPlgmState(state);
                break;

            } else if (VitaControls::getInstance().isButtonBack()) {
                if (!path.empty())
                    path.pop_back();

                list.clear();

                cpath = joinPathArr(path);
                idx = start = 0;

                if (cpath == "")
                    list = getAvailableMountPoints();
                else
                    readDirectory(cpath.c_str(), list);

            } else if (VitaControls::getInstance().isButtonEnter()) {
                std::string next = cpath == "" ? list[idx] : cpath+list[idx];

                if (list[idx] == ".." || isDir(next.c_str())) {
                    if (list[idx] == "..") {
                        if (!path.empty())
                            path.pop_back();

                    } else {
                        path.push_back(list[idx]);
                    }

                    cpath = joinPathArr(path);
                    idx = start = 0;

                    list.clear();

                    if (cpath == "")
                        list = getAvailableMountPoints();
                    else
                        readDirectory(cpath.c_str(), list);
                }
            }
            else if (VitaControls::getInstance().isButtonUp() || VitaControls::getInstance().isButtonAnalogUp())
                idx = (idx-1) <= 0 ? 0 : idx-1;
            else if (VitaControls::getInstance().isButtonDown() || VitaControls::getInstance().isButtonAnalogDown())
                idx = (idx+1) >= list.size() ? idx : idx+1;
        }

        start = getNewStartIndexForList(idx, start, end);
    }
}

void selectIDsUI(const std::vector<std::pair<std::string, std::string>> &appsList, std::vector<int> &selectedIDs) {
    const char *title = "Select Application IDs";
    const char *help = "[ ] Multi-select IDs,  O Cancel,  X Confirm";
    int titleHalfW = vita2d::getInstance().getTextWidth(title) / 2;
    int helpHalfW = vita2d::getInstance().getTextWidth(help) / 2;
    float x = 25;
    float yPad = 25;
    int start = 0;
    int idx = 0;

    while (true) {
        int end = start + 17;
        float y = 80;

        vita2d::getInstance().startRendering();
        vita2d::getInstance().setTextColor(vita2d::COLOR::GREEN);
        vita2d::getInstance().drawText(vita2d::getInstance().getScreenXCenter() - titleHalfW, 20, title);
        vita2d::getInstance().setTextColor(vita2d::COLOR::WHITE);
        vita2d::getInstance().setObjectColor(vita2d::COLOR::LIGHTBLUE);

        if (appsList.size() == 0) {
            vita2d::getInstance().drawText(x, y, "Unable to load applications list from database!");

        } else {
            if (start > 0)
                vita2d::getInstance().drawText(x, y-yPad, "+");

            for (int i=start,l=appsList.size(); i<l && i<end; ++i) {
                std::string str = appsList[i].first + " - [ " + appsList[i].second + " ]";

                if (std::find(selectedIDs.begin(), selectedIDs.end(), i) != selectedIDs.end())
                    vita2d::getInstance().drawCircle(10, y-8, 8.0f);

                if (i == idx) {
                    vita2d::getInstance().setTextColor(vita2d::COLOR::GREEN);
                    vita2d::getInstance().drawText(x, y, str);
                    vita2d::getInstance().setTextColor(vita2d::COLOR::WHITE);

                } else {
                    vita2d::getInstance().drawText(x, y, str);
                }

                y += yPad;
            }

            if (end < appsList.size())
                vita2d::getInstance().drawText(x, y, "+");
        }

        vita2d::getInstance().drawText(vita2d::getInstance().getScreenXCenter() - helpHalfW, y+yPad, help);
        vita2d::getInstance().endRendering();
        VitaControls::getInstance().getKeys();

        if (VitaControls::getInstance().isButtonSquare()) {
            std::vector<int>::iterator it = std::find(selectedIDs.begin(), selectedIDs.end(), idx);

            if (it != selectedIDs.end())
                selectedIDs.erase(it);
            else
                selectedIDs.push_back(idx);

        } else if (VitaControls::getInstance().isButtonBack()) {
            selectedIDs.clear();
            break;
        }
        else if (VitaControls::getInstance().isButtonEnter()) {
            if (selectedIDs.empty())
                selectedIDs.push_back(idx);

            break;

        } else if (VitaControls::getInstance().isButtonUp() || VitaControls::getInstance().isButtonAnalogUp())
            idx = (idx-1) <= 0 ? 0 : idx-1;
        else if (VitaControls::getInstance().isButtonDown() || VitaControls::getInstance().isButtonAnalogDown())
            idx = (idx+1) >= appsList.size() ? idx : idx+1;

        start = getNewStartIndexForList(idx, start, end);
    }
}

int main() {
    PLGM_STATE state = PLGM_STATE::CONFIG_UI;
    std::vector<std::pair<std::string, std::string>> appsList;
    TaiConfig conf;
    PlgmMenu menu;

    scePowerSetArmClockFrequency(444);

    conf.parse();
    Dialog::init();
    PluginsFolder::scanPluginsFolder();

    if (!initSqlite()) {
        printFatalError(PLGM_RET::EINIT_SQLITE);
        return 1;
    }

    loadApplicationsList(appsList);

    while (true) {
        switch (state) {
            case PLGM_STATE::OVERVIEW_UI:
                overviewUI(conf, menu, state);
                break;
            case PLGM_STATE::ENABLED_UI: {
                std::vector<std::string> enabled = getEnabledPluginsList(conf);

                enabledDisabledAllCommUI(conf, menu, PlgmMenu::MENU_MODE::ENABLED_UI, state, enabled, "Enabled Plugins", appsList);
            }
                break;
            case PLGM_STATE::DISABLED_UI: {
                std::vector<std::string> disabled = getDisabledPluginsList(conf);

                enabledDisabledAllCommUI(conf, menu, PlgmMenu::MENU_MODE::DISABLED_UI, state, disabled, "Disabled Plugins", appsList);
            }
                break;
            case PLGM_STATE::ALL_PLUGINS_UI:
                enabledDisabledAllCommUI(conf, menu, PlgmMenu::MENU_MODE::ALL_PLUGINS_UI, state, PluginsFolder::list, "All Plugins", appsList);
                break;
            case PLGM_STATE::FSBROWSER_UI:
                fileBrowserUI(conf, menu, state, appsList);
                break;
            default:
            case PLGM_STATE::CONFIG_UI:
                configUI(conf, menu, state);
                break;
        }
    }

    closeSqlite();
    return 0;
}