/*
	Plugins Manager
	Copyright (C) 2017-2022, kylon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <sstream>
#include <utility>
#include <psp2/sysmodule.h>
#include <psp2/kernel/threadmgr.h>
#include <psp2/power.h>

#include "include/enums.h"
#include "include/pluginsFolder.h"
#include "include/utils.h"
#include "include/sqlite3.h"

extern "C" int taiReloadConfig();

void trimString(std::string &str) {
    str.erase(
        std::remove_if(str.begin(), str.end(), [](unsigned char c){ return std::isspace(c); }), str.end()
    );
}

void trimNewLinesString(std::string &str) {
    str.erase(
        std::remove_if(str.begin(), str.end(), [](unsigned char c){ return c == '\n'; }), str.end()
    );
}

std::string joinPathArr(const std::vector<std::string> &vec) {
    if (vec.size() == 0)
        return "";

    std::vector<std::string>::const_iterator it = vec.begin();
    std::ostringstream sstream;

    sstream << *it;
    ++it;

    while (it != vec.end()) {
        sstream << *it;
        ++it;
    }

    return sstream.str();
}

std::string toLowerCase(const std::string &str) {
    std::string s = str;

    std::transform(s.begin(), s.end(), s.begin(), [](u_char c) -> u_char { return std::tolower(c); });

    return s;
}

bool containsStr(const std::string &str, const char *find) {
	return str.find(find) != std::string::npos;
}

bool containsStr(const std::string &str, const std::string &find) {
	return str.find(find) != std::string::npos;
}

PLGM_STATE switchToPrevPlgmState(PLGM_STATE cstate) {
    int _state = (int)cstate;

    _state = _state-1 == (int)PLGM_STATE::BEGIN ? (int)PLGM_STATE::END-1 : _state-1;

    return (PLGM_STATE)_state;
}

PLGM_STATE switchToNextPlgmState(PLGM_STATE cstate) {
    int _state = (int)cstate;

    _state = _state+1 == (int)PLGM_STATE::END ? (int)PLGM_STATE::BEGIN+1 : _state+1;

    return (PLGM_STATE)_state;
}

std::vector<std::string> getEnabledPluginsList(const TaiConfig &conf) {
    std::vector<std::string> rawConf = conf.getRawConfig();
    std::vector<std::string> ret;

    for (const std::string &plugin: PluginsFolder::list) {
        std::string l1 = toLowerCase(plugin);

        for (const std::string &line: rawConf) {
            if (line.at(0) == '*')
                continue;

            std::string l2 = toLowerCase(line);

            if (!containsStr(l2, l1))
                continue;

            ret.push_back(plugin);
            break;
        }
    }

    return ret;
}

std::vector<std::string> getDisabledPluginsList(const TaiConfig &conf) {
    std::vector<std::string> rawConf = conf.getRawConfig();
    std::vector<std::string> ret;

    for (const std::string &plugin: PluginsFolder::list) {
        std::string l1 = toLowerCase(plugin);
        bool found = false;

        for (const std::string &line: rawConf) {
            if (line.at(0) == '*')
                continue;

            std::string l2 = toLowerCase(line);

            if (!containsStr(l2, l1))
                continue;

            found = true;
            break;
        }

        if (!found)
            ret.push_back(plugin);
    }

    return ret;
}

int getNewStartIndexForList(int idx, int start, int end) {
    return idx < start ? start-1 : (idx >= end ? start+1 : start);
}

void loadApplicationsList(std::vector<std::pair<std::string, std::string>> &out) {
    sqlite3 *db = NULL;
    sqlite3_stmt *stmt;
    int ret;

    ret = sqlite3_open_v2("ur0:shell/db/app.db", &db, SQLITE_OPEN_READONLY, NULL);
    if (ret != SQLITE_OK) {
        sqlite3_close(db);
        return;
    }

    ret = sqlite3_prepare_v2(db, "select title, titleId from tbl_appinfo_icon where type != 5", -1, &stmt, NULL);
    if (ret != SQLITE_OK) {
        sqlite3_close(db);
        return;
    }

    while (sqlite3_step(stmt) == SQLITE_ROW) {
        char title[250] = {0};
        char id[20] = {0};
        std::string ftitle;

        sqlite3_snprintf(sizeof(title), title, "%s", sqlite3_column_text(stmt, 0));
        sqlite3_snprintf(sizeof(id), id, "*%s", sqlite3_column_text(stmt, 1));

        ftitle.assign(title);
        trimNewLinesString(ftitle);
        out.push_back(std::make_pair(id, ftitle));
    }

    sqlite3_finalize(stmt);
    sqlite3_close(db);

    out.insert(out.begin(), {
            std::make_pair("*ALL", "All user-mode processes"),
            std::make_pair("*KERNEL", "OS Kernel"),
            std::make_pair("*main", "Shell/LiveArea")
            });
}

bool initSqlite() {
    sceSysmoduleLoadModule(SCE_SYSMODULE_SQLITE);

    return sqlite_init() == SQLITE_OK;
}

bool closeSqlite() {
    bool ret = sqlite_exit() == SQLITE_OK;

    sceSysmoduleUnloadModule(SCE_SYSMODULE_SQLITE);
    return ret;
}

void saveConfig(TaiConfig &conf) {
    PLGM_RET ret = conf.write();

    if (ret != PLGM_RET::SUCCESS) {
        Dialog::showMessageDialog("Error: Unable to write config!", Dialog::DIALOG_BUTTON_TYPE::BUTTONS_OK);
        return;
    }

    taiReloadConfig();
    Dialog::showMessageDialog("Success!", Dialog::DIALOG_BUTTON_TYPE::BUTTONS_OK);

    if (!conf.requiresRebootToApply)
        return;

    Dialog::DIALOG_RESULT dret = Dialog::showMessageDialog(
            "A reboot is required to apply the changes.\n\nDo you want to reboot now?",
            Dialog::DIALOG_BUTTON_TYPE::BUTTONS_YESNO);

    if (dret == Dialog::DIALOG_RESULT::YES) {
        sceKernelDelayThread(80000);
        scePowerRequestColdReset();
    }
}

bool reloadConfig(TaiConfig &conf) {
    Dialog::DIALOG_RESULT dret = Dialog::showMessageDialog(
                                    "Reload your config from disk.\n\nAll unsaved changes will be lost.\n\nDo you want to reload your config?",
                                    Dialog::DIALOG_BUTTON_TYPE::BUTTONS_YESNO);
    PLGM_RET ret;

    if (dret == Dialog::DIALOG_RESULT::NO)
        return false;

    ret = conf.reload();
    if (ret != PLGM_RET::SUCCESS) {
        Dialog::showMessageDialog("Error: Unable to load config from disk!", Dialog::DIALOG_BUTTON_TYPE::BUTTONS_OK);
        return false;
    }

    conf.requiresRebootToApply = false;

    return true;
}

bool resetConfig(TaiConfig &conf) {
    Dialog::DIALOG_RESULT dret = Dialog::showMessageDialog(
                                    "This will reset your config to TaiHen default.\n\nDo you want to reset your config?",
                                    Dialog::DIALOG_BUTTON_TYPE::BUTTONS_YESNO);

    if (dret == Dialog::DIALOG_RESULT::NO)
        return false;

    conf.reset();
    return true;
}

std::string errorString(PLGM_RET err) {
    switch (err) {
        case PLGM_RET::EINIT_SQLITE:
            return "Sqlite module error";
        default:
            return "Unknown error";
    }
}